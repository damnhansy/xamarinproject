﻿	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using System.Reflection;
	using System.Runtime.CompilerServices;
	using System.Windows.Input;
	using Xamarin.Forms;

	namespace Voyager
	{
	    public abstract class BaseViewModel : INotifyPropertyChanged
	    {
			public BaseViewModel()
			{
				this.commands =
					this.GetType().GetTypeInfo().DeclaredMethods
					.Where(dm => dm.Name.EndsWith(EXECUTECOMMAND_SUFFIX))
					.ToDictionary(k => GetCommandName(k), v => GetCommand(v));
			}

	        //
			public event PropertyChangedEventHandler PropertyChanged;

			protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
			{
				var handler = PropertyChanged;
				if (handler != null)
					handler(this, new PropertyChangedEventArgs(propertyName));


			}


	        //for bindable property
			private Dictionary<string, object> properties = new Dictionary<string, object>();

			protected void SetValue<T>(T value, [CallerMemberName] string propertyName = null)
			{
				if (!properties.ContainsKey(propertyName))
				{
					properties.Add(propertyName, default(T));
				}

				var oldValue = GetValue<T>(propertyName);
				if (!EqualityComparer<T>.Default.Equals(oldValue, value))
				{
					properties[propertyName] = value;
					OnPropertyChanged(propertyName);
				}
			}

			protected T GetValue<T>([CallerMemberName] string propertyName = null)
			{
				if (!properties.ContainsKey(propertyName))
				{
					return default(T);
				}
				else
				{
					return (T)properties[propertyName];
				}
			}



			//for bindable property
			private Dictionary<string, System.Windows.Input.ICommand> commands = new Dictionary<string, ICommand>();

			private const string EXECUTECOMMAND_SUFFIX = "_Command";
			private const string CANEXECUTECOMMAND_SUFFIX = "_CanExecuteCommand";

			

			private string GetCommandName(MethodInfo mi)
			{
				return mi.Name.Replace(EXECUTECOMMAND_SUFFIX, "");
			}

			private ICommand GetCommand(MethodInfo mi)
			{
				var canExecute = this.GetType().GetTypeInfo().GetDeclaredMethod(GetCommandName(mi) + CANEXECUTECOMMAND_SUFFIX);
				var executeAction = (Action<object>)mi.CreateDelegate(typeof(Action<object>), this);
				var canExecuteAction = canExecute != null ? (Func<object, bool>)canExecute.CreateDelegate(typeof(Func<object, bool>), this) : state => true;
				return new Command(executeAction, canExecuteAction);
			}

			public ICommand this[string name]
			{
				get
				{
					return commands[name];
				}
			}

		}
	}
