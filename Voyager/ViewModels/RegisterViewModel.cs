﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
namespace Voyager
{
    public class RegisterViewModel :BaseViewModel
    {
        public INavigation _navigation;
        public RegisterViewModel(INavigation navigation)
        {
            _navigation = navigation;
            IsError = false;
        }
		public Boolean IsError
		{

			get { return GetValue<Boolean>(); }
			set { SetValue(value); }
		}


		public string FirstName
		{

			get { return GetValue<string>(); }
			set { SetValue(value); }
		}


		public string Email
		{

			get { return GetValue<string>(); }
			set { SetValue(value); }
		}
		public string Account
		{

			get { return GetValue<string>(); }
			set { SetValue(value); }
		}


		public string Mobile
		{

			get { return GetValue<string>(); }
			set { SetValue(value); }
		}

		public ICommand SubmitBtnClicked
		{
			get
			{
				return new Xamarin.Forms.Command(() =>
				{
					Debug.WriteLine("LoginBtnClicked");
					validateUserData();
				});
			}
		}
        public  void validateUserData(){
            
			bool isEmail = System.Text.RegularExpressions.Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
			if (FirstName.Length == 0 || Email.Length == 0 || Account.Length == 0 || Mobile.Length == 0 ){

                IsError = true;

            }
			else if (!isEmail || Email.Length == 0)
			{

				IsError = true;
			}
            else{

				App.Current.MainPage.DisplayAlert("You are succesfully registerd!.", "Success", "OK");
				_navigation.PopAsync(true);
            }

        }

		
    }
}
