﻿	using System;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Runtime.CompilerServices;
	using System.Windows.Input;
	using Xamarin.Forms;

	namespace Voyager
	{
	    public class LoginViewModel:BaseViewModel
	    {
	        private Xamarin.Forms.INavigation _navigation;
		private ILoginManager loginManager;
	        public LoginViewModel(INavigation navigation,ILoginManager ilm)
	        {
	            _navigation = navigation;
	            IsError = false;
			loginManager = ilm;
	        }

			
	       
			public Boolean IsError
			{

				get { return GetValue<Boolean>(); }
				set { SetValue(value); }
			}


			public string Username
			{

				get { return GetValue<string>(); }
				set { SetValue(value); }
			}


			public string Password
			{

				get { return GetValue<string>(); }
				set { SetValue(value); }
			}


			
	        public void checkusernameAndPassword(){

            if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Username)){

				IsError = true;
	            }
	            else{

					//check from rest API

				// REMEMBER LOGIN STATUS!
				App.Current.Properties["IsLoggedIn"] = true;
				loginManager.ShowMainPage();


				}

	        }


	        public void LoginBtnClicked_Command(Object state){
				checkusernameAndPassword();
			}

			public void ForgotPasswordBtnClicked_Command(Object state)
			{
				_navigation.PushAsync(new ForgotPasswordPage());

			}

			public void RegisterBtnClicked_Command(Object state)
			{
				_navigation.PushAsync(new RegisterPage());

			}

			
	    }
	}
