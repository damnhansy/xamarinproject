﻿	using System;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Runtime.CompilerServices;
	using System.Windows.Input;
	using Xamarin.Forms;

	namespace Voyager
	{
	    public class ForgotPasswordViewModel : BaseViewModel
	    {
	        public INavigation _navigation;
	        public ForgotPasswordViewModel(INavigation navigation)
	        {
	            _navigation = navigation;
	            IsError = false;

	        }


	        public Boolean IsError{

	            get { return GetValue<Boolean>(); }
	            set {SetValue(value);}
	        }

			public string Email
			{

				get { return GetValue<string>(); }
				set { SetValue(value); }
			}

	        public void ForgotPassBtnClicked_Command(Object state){
				checkEmailFormat();


			}

	        public void checkEmailFormat(){

	            bool isEmail = System.Text.RegularExpressions.Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
	            if (!isEmail || Email.Length == 0)
	            {

	                IsError = true;
	            }
	            else
	            {

	                //call rest api for forgot password
	              //  MessageCenter.Send(this, "MyAlertName", "My actual alert content, or an object if you want");


					 App.Current.MainPage.DisplayAlert("Please check your email to reset password.", "Success", "OK");
	                _navigation.PopAsync(true);

				}
			}
			

		}
	}
