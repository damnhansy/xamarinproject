﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace Voyager
{
    public partial class MenuPage : ContentPage
    {
        public MenuViewModel ViewModel;
        public MenuPage()
        {
            InitializeComponent();
            ViewModel = new MenuViewModel(this.Navigation);
			Icon = "slideout.png";
            Title = "Test";
			BindingContext = ViewModel;
            BackgroundColor = Color.Red;
            ObservableCollection<SettingList> Items;
			Items = new ObservableCollection<SettingList>();
            Items.Add(new SettingList(){ Index =1,Name = "BOOK NEW RESERVATION",Image ="account.png"});
            Items.Add(new SettingList() { Index =2,Name  = "CALL LIMOLINK", Image = "account.png" });
            Items.Add(new SettingList() { Index =3,Name= "SELECT ACCOUNT", Image = "account.png" });
            Items.Add(new SettingList() {Index=4, Name = "PUSH NOTIFICATION", Image = "account.png" });
            Items.Add(new SettingList() {Index = 5, Name = "HELP", Image = "account.png" });
            Items.Add(new SettingList() {Index =6, Name = "LOGOFF", Image = "account.png" });



            BackgroundColor = Color.Black;
			InitializeComponent();
            var logoImage = new Image
            {
                HorizontalOptions = LayoutOptions.Center,
                Source = "LimoLink_Logo_all_white_60Hight.png"



		};
			var header = new Label
			{
				Text = "VOYAGER APP",
				Font = Font.SystemFontOfSize(17),
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White

			};

			var listView = new ListView(ListViewCachingStrategy.RecycleElement)
			{
				ItemsSource = Items,
                ItemTemplate = new DataTemplate(typeof(SettingCell))

			//ItemTemplate = new DataTemplate(() =>
				//{
    //                var nativeCell = new ImageCell();
    //                nativeCell.SetBinding(ImageCell.TextProperty, "Name");
    //                nativeCell.SetBinding(ImageCell.ImageSourceProperty, "Image");

				//	return nativeCell;
				//})
			};
            listView.SeparatorVisibility = SeparatorVisibility.None;
            listView.RowHeight = 70;
            listView.ItemSelected += (sender, e) =>
			{
                if ("LOGOFF" == Items[Items.Count - 1].Name){

					Debug.WriteLine("log off clicked");
					MenuViewModel model =  (MenuViewModel)this.ViewModel;
                    model.LogOffBtnClicked();
                }


			};
            this.Content = new StackLayout { 
                Children = {logoImage, header,listView}


            };

			//	ListView settingList = new ListView
			//	{
			//		ItemsSource = titles,
			//		BackgroundColor = Color.Black,
			//		HasUnevenRows = false,

			//		ItemTemplate = new DataTemplate(() =>
			//		{
			//			// Create views with bindings for displaying each property.
			//			Label titleLabel = new Label();
			//			titleLabel.WidthRequest = 200;
			//			titleLabel.HeightRequest = 50;
			//                  titleLabel.TextColor = Color.Red;
			//			titleLabel.SetBinding(Label.TextProperty, "Title");
			//			BoxView boxView = new BoxView();

			//			return new ViewCell
			//			{
			//				//Height = 200,
			//				View = new StackLayout
			//				{
			//					Padding = new Thickness(0, 0),
			//					Orientation = StackOrientation.Horizontal,
			//                          BackgroundColor = Color.Blue,
			//					Children = {
			//				boxView,
			//                              titleLabel
			//			}
			//				}
			//			};
			//		})

			//	};

			//	this.Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 5);
			//	settingList.Footer = new StackLayout { };
			//	// Build the page.
			//	this.Content = new StackLayout
			//	{
			//              BackgroundColor = Color.Brown,
			//		Children = {
			//			logoImage,
			//	header,
			//	settingList
			//}
			//};
		}
    }

    public class SettingList{
		public int Index { get; set; }
		public string Name { set; get; }
        public string Image { set; get; }
        public Color RowColor { get { if (Index % 2 == 0) return Color.Black; else { return Color.FromHex("#2A2A2A"); } } }

	}
}
