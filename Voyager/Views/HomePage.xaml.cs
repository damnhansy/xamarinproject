﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Voyager
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            BindingContext = new HomeViewModel(this.Navigation);
			NavigationPage.SetTitleIcon(this, "LimoLink_Logo_all_white_60Hight.png");
          
			//BackgroundColor = Color.Gray;
            //var viewmodel = new HomeViewModel(this.Navigation);
            //var list = new ListView();
            //         list.ItemsSource = viewmodel.ReservationList;
            //var cell = new DataTemplate(typeof(ImageCell));
            //cell.SetBinding(ImageCell.ImageSourceProperty, "PersonName");
            //cell.SetBinding(ImageCell.TextProperty, "City");
            //cell.SetBinding(ImageCell.DetailProperty, "PickUpDate");
            //list.ItemTemplate = cell;
            //list.ItemTapped += (sender, args) =>
            //{
            //	//var city = args.Item as City;
            //	//if (city == null)
            //	//	return;
            //	//Navigation.PushAsync(new DetailPage(city));
            //	//list.SelectedItem = null;
            //};
            //Content = list;
            lstView.ItemsSource = new HomeViewModel(this.Navigation).ReservationList;
		}
    }

	
}
