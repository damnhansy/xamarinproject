﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Voyager
{
    public partial class ForgotPasswordPage : ContentPage
    {
        public ForgotPasswordPage()
        {
            InitializeComponent();
           // NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new ForgotPasswordViewModel(this.Navigation);

		}
    }
}
