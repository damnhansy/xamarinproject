﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Voyager
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
            BindingContext = new RegisterViewModel(this.Navigation);
			//NavigationPage.SetHasNavigationBar(this, false);

		}
    }
}
