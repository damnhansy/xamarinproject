﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Voyager
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage(ILoginManager ilm)
        {
            InitializeComponent();
            BindingContext = new LoginViewModel(this.Navigation,ilm);
            NavigationPage.SetHasNavigationBar(this, false);

        }
    }
}
