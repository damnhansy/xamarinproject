﻿using Xamarin.Forms;

namespace Voyager
{
    public partial class App : Application , ILoginManager
    {
		public static App Current;

        public App()
        {
            InitializeComponent();

			Current = this;
			var isLoggedIn = Properties.ContainsKey("IsLoggedIn") ? (bool)Properties["IsLoggedIn"] : false;

            // we remember if they're logged in, and only display the login page if they're not
            if (isLoggedIn)
                MainPage = new MainPage();
            else
                MainPage = new NavigationPage(new LoginPage(this));
        }

		public void ShowMainPage()
		{
            MainPage = new MainPage();
		}

		public void Logout()
		{
			Properties["IsLoggedIn"] = false; // only gets set to 'true' on the LoginPage
			MainPage = new NavigationPage(new LoginPage(this));
		}
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

       
    }
}
