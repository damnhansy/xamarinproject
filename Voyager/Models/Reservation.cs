﻿using System;
using Xamarin.Forms;

namespace Voyager
{
    public class Reservation
    {
		public int Index { get; set; }
		public string PersonName { set; get; }
		public string ReservationDate { set; get; }
		public string PickUpTime { set; get; }
		public string City { set; get; }
		public Color RowColor { get { if (Index % 2 == 0) return Color.FromHex("#EEEEEE"); else { return Color.White; } } }

	}
}
