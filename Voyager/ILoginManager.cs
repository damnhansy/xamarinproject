﻿using System;
namespace Voyager
{
	public interface ILoginManager
	{
		void ShowMainPage();
		void Logout();
	}
}
