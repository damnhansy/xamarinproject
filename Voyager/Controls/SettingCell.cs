﻿using System;
using Xamarin.Forms;

namespace Voyager
{
    public class SettingCell : ViewCell
    {
		
        public  SettingCell(){
			var image = new Image()
			{

				HeightRequest = 50,
                WidthRequest = 50
			};
            var imageStack = new StackLayout
            {
                BackgroundColor = Color.White,
                HeightRequest = 50,
                WidthRequest = 50,
                Children={image}

            };

            var text = new Label()
            {
                TextColor = Color.White,
                FontSize = 20
            };

            var stack = new StackLayout
            {
                Padding = 5,
                Orientation = StackOrientation.Horizontal,
                Children = {
                    imageStack, new StackLayout(){ Padding = 5, Children = {text}}
                }
                                       

            };

            text.SetBinding(Label.TextProperty, "Name");
            image.SetBinding(Image.SourceProperty, "Image");
            stack.SetBinding(StackLayout.BackgroundColorProperty, "RowColor");
            this.View = stack;
        }
		
    }
}
