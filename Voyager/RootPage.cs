﻿using System;
using Xamarin.Forms;

namespace Voyager
{
    public class RootPage : MasterDetailPage
    {
        public RootPage()
        {
			MasterBehavior = MasterBehavior.Popover;

		}
    }
}
