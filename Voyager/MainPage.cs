﻿using System;
using Xamarin.Forms;

namespace Voyager
{
	public class MainPage : MasterDetailPage

    {
        public MainPage()
        {
			Master = new MenuPage();
            Detail = new NavigationPage(new HomePage())
            {
				BarBackgroundColor = Color.FromHex("#72978D"),
				BarTextColor = Color.White

            };
        }
    }
}
